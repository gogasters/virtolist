package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"github.com/pkg/profile"
	"gitlab.com/gogasters/virtolist/internal/network"
	"gitlab.com/gogasters/virtolist/internal/position"
	"image"
	"image/color"
	"log"
	"math"
)

/*
Типы
*/

/*
Константы
*/

const (
	MaxX = 400
	MaxY = 300
	MaxD = 3

	PPROF = false
)

/*
Переменные
*/

var (
	localCanvasImage  *ebiten.Image
	sharedCanvasImage *ebiten.Image
	brushImage        *ebiten.Image
	lastLocalPoint    [2]float64 // last shared point
	lastSharedPoint   [2]float64 //last local point

	connection = network.Connection{Status: "needIp"}
)

/*
Функции
*/
func dropLastPoint() {
	lastLocalPoint[0] = -1
}

func initLastPoint(x, y float64) {
	if lastLocalPoint[0] == -1 {
		lastLocalPoint[0], lastLocalPoint[1] = x, y
		lastSharedPoint = lastLocalPoint
	}
}

func pointsDistance(x1, y1, x2, y2 float64) float64 {
	return math.Sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
}

func pointsShift(x1, y1, x2, y2 float64) (float64, float64) {
	percent := MaxD / pointsDistance(x1, y1, x2, y2)
	xShifted := x1 + (x2-x1)*percent
	yShifted := y1 + (y2-y1)*percent
	return xShifted, yShifted
}

func PaintShared(screen *ebiten.Image, opt *ebiten.DrawImageOptions, x int, y int) {
	xf, yf := float64(x), float64(y)
	for lp := lastSharedPoint; pointsDistance(lp[0], lp[1], xf, yf) > MaxD; {
		lp[0], lp[1] = pointsShift(lp[0], lp[1], xf, yf)
		opt.GeoM.Translate(lp[0], lp[1])
		localCanvasImage.DrawImage(brushImage, opt)
		opt.GeoM.Translate(-lp[0], -lp[1])
	}
	opt.GeoM.Translate(xf, yf)
	sharedCanvasImage.DrawImage(brushImage, opt)
	lastSharedPoint[0], lastSharedPoint[1] = xf, yf
}

func PaintLocal(screen *ebiten.Image, opt *ebiten.DrawImageOptions, x, y float64) {
	if connection.Status == "connected" {
		network.WriteToOutChan(position.Point{X: int(x), Y: int(y)})
	}
	xf, yf := float64(x), float64(y)
	for lp := lastLocalPoint; pointsDistance(lp[0], lp[1], xf, yf) > MaxD; {
		lp[0], lp[1] = pointsShift(lp[0], lp[1], xf, yf)
		opt.GeoM.Translate(lp[0], lp[1])
		localCanvasImage.DrawImage(brushImage, opt)
		opt.GeoM.Translate(-lp[0], -lp[1])
	}
	opt.GeoM.Translate(xf, yf)
	localCanvasImage.DrawImage(brushImage, opt)
	lastLocalPoint[0], lastLocalPoint[1] = xf, yf
}

func UpdateShared(screen *ebiten.Image) {
	point := network.ReadFromInChan()
	opts := ebiten.DrawImageOptions{}
	PaintShared(screen, &opts, point.X, point.Y)
}

func update(screen *ebiten.Image) error {

	screen.Fill(color.NRGBA{0x80, 0x80, 0x80, 0xff})

	x, y := ebiten.CursorPosition()
	opts := ebiten.DrawImageOptions{}
	if (x >= 0 && y >= 0) && (x <= MaxX && y <= MaxY) {
		if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			initLastPoint(float64(x), float64(y))
			PaintLocal(screen, &opts, float64(x), float64(y))
		} else {
			dropLastPoint()
		}
	}

	connection.UpdateMsg()
	if connection.Status == "connected" {
		go UpdateShared(screen)
	}

	ebitenutil.DebugPrint(screen, fmt.Sprintf("Your IP: %s | Your Port: %s", connection.LocalIP, connection.LocalPort))
	if connection.Status != "connected" {
		ebitenutil.DebugPrint(screen, fmt.Sprintf("\n%s %s", connection.Msg, connection.Tmp))
	} else {
		ebitenutil.DebugPrint(screen, fmt.Sprintf("\n%s", connection.Msg))
	}

	screen.DrawImage(localCanvasImage, nil)
	screen.DrawImage(sharedCanvasImage, nil)
	return nil
}

func main() {
	lastSharedPoint[0], lastSharedPoint[1] = -1, -1
	lastLocalPoint[0], lastLocalPoint[1] = -1, -1
	if PPROF {
		defer profile.Start(profile.ProfilePath("."), profile.NoShutdownHook, profile.MemProfile, profile.CPUProfile).Stop()
	}
	go connection.TCPServer()
	go connection.ReadFromKb()

	var err error
	const a0, a1, a2 = 0x40, 0xc0, 0xff
	pixels := []uint8{
		a0, a1, a1, a0,
		a1, a2, a2, a1,
		a1, a2, a2, a1,
		a0, a1, a1, a0,
	}
	brushImage, err = ebiten.NewImageFromImage(&image.Alpha{
		Pix:    pixels,
		Stride: 4,
		Rect:   image.Rect(0, 0, 4, 4),
	}, ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}
	if sharedCanvasImage, err = ebiten.NewImage(MaxX, MaxY, ebiten.FilterNearest); err != nil {
		log.Fatal(err)
	}
	if localCanvasImage, err = ebiten.NewImage(MaxX, MaxY, ebiten.FilterNearest); err != nil {
		log.Fatal(err)
	}
	ebiten.Run(update, MaxX, MaxY, 2, "Виртолист")
}
