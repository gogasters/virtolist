package position

/*
Типы
*/

type Point struct {
	X int
	Y int
}

type PointChan struct {
	Chan chan Point
}

/*
Функции
*/

func (this *PointChan) WriteToPointChan(point Point) {
	this.Chan <- point
}

func (this *PointChan) ReadFromPointChan() Point {
	return <-this.Chan
}
