package network

import (
	"bufio"
	"bytes"
	"encoding/json"
	"github.com/hajimehoshi/ebiten"
	"gitlab.com/gogasters/virtolist/internal/position"
	"log"
	"math/rand"
	"net"
	"strconv"
	"time"
)

/*
Типы
*/

type Connection struct {
	RemoteIP   string
	RemotePort string
	LocalIP    string
	LocalPort  string
	Tmp        string

	Msg    string
	Status string // needIp, needPort, connected

	IsBackConnect bool
}

/*
Константы
*/
const (
	LocIP = "127.0.0.1"
	LogLevel = 1
)

/*
Переменные
*/
var (
	keyRemotes = map[ebiten.Key]string{
		ebiten.KeyPeriod:    ".",
		ebiten.KeyEnter:     "Enter",
		ebiten.KeyBackspace: "Backspace",
	}

	inChan  = position.PointChan{Chan: make(chan position.Point, 250)}
	outChan = position.PointChan{Chan: make(chan position.Point, 250)}
)

/*
Функции
*/

//Функция-посредник для использования в пакете main
func ReadFromInChan() position.Point {
	return inChan.ReadFromPointChan()
}

//Функция-посредник для использования в пакете main
func ReadFromOutChan() position.Point {
	return outChan.ReadFromPointChan()
}

//Функция-посредник для использования в пакете main
func WriteToInChan(point position.Point) {
	inChan.WriteToPointChan(point)
}

//Функция-посредник для использования в пакете main
func WriteToOutChan(point position.Point) {
	outChan.WriteToPointChan(point)
}

//Запускается в main()
func (this *Connection) TCPServer() {

	this.LocalIP = LocIP
	rand.Seed(time.Now().UnixNano())
	this.LocalPort = strconv.Itoa(rand.Intn(30000) + 30000)
	listener, err := net.Listen("tcp", this.LocalIP+":"+this.LocalPort)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		defer conn.Close()
		go TCPServerHandler(conn, this)
	}
}

func TCPServerHandler(conn net.Conn, this *Connection) {
	var point position.Point
	var remoteNetSettings Connection
	log.Printf("Start Handler: %s:%s\n", this.LocalIP, this.LocalPort)

	/*
		Считывание данных для обратного подключения.
		IsBackConnection = (true|false) - значение указывает
		инициализировать или нет обратное подключение
	*/
	jremoteNetSettings, _ := bufio.NewReader(conn).ReadBytes('}')
	decoder := json.NewDecoder(bytes.NewReader(jremoteNetSettings))
	if err := decoder.Decode(&remoteNetSettings); err != nil {
		log.Println("network(1): ", err)
	} else {
		if remoteNetSettings.IsBackConnect {
			log.Printf("Send back connection to %s:%s\n", remoteNetSettings.RemoteIP, remoteNetSettings.RemotePort)
			//Если инициализируется обратное подключение...
			this.RemoteIP = remoteNetSettings.RemoteIP
			this.RemotePort = remoteNetSettings.RemotePort
			this.IsBackConnect = false
			go this.TCPClient()
		}
		for {
			//Считывание значений (X:Y) из
			jpoint, _ := bufio.NewReader(conn).ReadBytes('}')
			if LogLevel >= 3 {
				log.Printf("(Server) Receive via TCP %s\n", jpoint)
			}
			decoder := json.NewDecoder(bytes.NewReader(jpoint))
			if err := decoder.Decode(&point); err != nil {
				log.Println("network(2): ", err)
			}
			inChan.WriteToPointChan(point)
			if LogLevel >= 4 {
				log.Printf("(Server) Write to InChan %q\n", point)
			}
		}
	}
}

/*
Функция подключения к TCP-серверу
Запускается в двух местах программы:
1. Функция ReadFromKb()
IsBackConnect = true

2. Функция TCPServerHandler()
IsBackConnect = false
*/
func (this *Connection) TCPClient() {

	conn, err := net.Dial("tcp", this.RemoteIP+":"+this.RemotePort)
	if err != nil {
		log.Println("network(3): ", err)
		this.Status = "needIp"
	} else {
		log.Printf("Connect to remote server %s:%s\n", this.RemoteIP, this.RemotePort)
		this.Status = "connected"

		//Отправка своего ip-адреса и порта для обратного соединения
		localNetSettings, err := json.Marshal(Connection{
			RemoteIP:      this.LocalIP,
			RemotePort:    this.LocalPort,
			IsBackConnect: this.IsBackConnect,
		})
		if err != nil {
			log.Println("network(4): ", err)
			//Здесь обработка исключения
		}

		_, err = conn.Write([]byte(localNetSettings))
		if err != nil {
			log.Println("network(5): ", err)
		}
		//Подключение к серверу/Работа с сервером
		for this.Status == "connected" {
			point, err := json.Marshal(outChan.ReadFromPointChan())
			if err != nil {
				log.Println("network(6): ", err)
				continue
			}
			if LogLevel >= 4 {
				log.Printf("(Client) Read from outChan %s\n", point)
			}

			_, err = conn.Write([]byte(point))
			if err != nil {
				log.Println("network(7): ", err)
			}
			if LogLevel >= 3 {
				log.Printf("(Client) Send via TCP %s\n", point)
			}
		}
		this.Status = "needIp"
		log.Printf("Disconnect from %s:%s\n", this.RemoteIP, this.RemotePort)
	}
}

func (this *Connection) UpdateMsg() {
	switch this.Status {
	case "needIp":
		this.Msg = "Enter remote IP:"
	case "needPort":
		this.Msg = "Enter remote Port:"
	case "connected":
		this.Msg = "Connected ('Enter' to disconnect)"
	}
}

//Запускается в main()
func (this *Connection) ReadFromKb() {
	for {
		for i := 0; i <= 9; i++ {
			if ebiten.IsKeyPressed(ebiten.Key(i) + ebiten.Key0) {
				this.Tmp += strconv.Itoa(i)
			}
			time.Sleep(7 * time.Millisecond)
		}
		for key, name := range keyRemotes {
			if ebiten.IsKeyPressed(key) {
				switch name {
				case ".":
					this.Tmp += "."
				case "Colon":
					this.Tmp += ":"
				case "Enter":
					switch this.Status {
					case "needIp":
						this.RemoteIP = this.Tmp
						this.Tmp = ""
						this.Status = "needPort"
					case "needPort":
						this.RemotePort = this.Tmp
						this.Tmp = ""
						this.IsBackConnect = true
						go this.TCPClient()
					case "connected":
						this.Status = "needIp"
					}
				case "Backspace":
					if len(this.Tmp) > 0 {
						this.Tmp = this.Tmp[:len(this.Tmp)-1]
					}
				}
			}
			time.Sleep(7 * time.Millisecond)
		}
	}
}
